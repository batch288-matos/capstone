const Carts = require('../models/cartsModel.js');
const Users = require('../models/usersModel.js');
const Products = require('../models/productsModel.js');
const Orders = require('../models/ordersModel.js');
const auth = require('../auth.js');


// Add products to cart
module.exports.addProducts = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const productId = request.params._id;

	Products.findById(request.params._id)
	.then(result => {
		let newCart = new Carts({
			userId: userData.id,
			products: [{
				productId: request.params._id,
			}],
			subTotal: result.price,
			totalAmount: result.price
		});

		newCart.save()
		.then(save => response.send('You have successfully added the product to your cart!'))
		.catch(error => response.send(error));
	})
	.catch(error => response.send(errr));
};