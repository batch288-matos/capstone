const Users = require('../models/usersModel.js');
// const Orders = require('../models/ordersModel.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

// Admin registration
module.exports.adminRegistration = (request, response) => {
	Users.findOne({email: request.body.email})
	.then(result => {
		if(!result){
			let newUser = new Users({
				email: request.body.email,
				password: bcrypt.hashSync(request.body.password, 10),
				isAdmin: request.body.isAdmin
			});

			newUser.save()
			.then(saved => response.send(`${request.body.email} is now registered!`))
			.catch(error => response.send(error));
		}
		else{
			return response.send(`${request.body.email} is already taken. Try logging in or use different email in signing up!`);
		};
	})
	.catch(error => response.send(error));
};

// User registration
module.exports.userRegistration = (request, response) => {
	Users.findOne({email: request.body.email})
	.then(result => {
		if(!result){
			let newUser = new Users({
				firstName: request.body.firstName,
				lastName: request.body.lastName,
				email: request.body.email,
				password: bcrypt.hashSync(request.body.password, 10),
				mobileNumber: request.body.mobileNumber
			});

			newUser.save()
			.then(saved => response.send(true))
			.catch(error => response.send(false));
		}
		else{
			return response.send(false);
		};
	})
	.catch(error => response.send(false));
};

// Login
module.exports.login = (request, response) => {
	Users.findOne({email: request.body.email})
	.then(result => {
		if(!result){
			return response.send(false);
		}
		else{
			let isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);
			
			if(isPasswordCorrect){
				return response.send({
					token: auth.createAccessToken(result)
				});
			}
			else{
				return response.send(false);
			};
		};
	})
	.catch(error => response.send(false));
};

// Retrieving user details
// module.exports.getUserDetails = (request, response) => {
// 	const userData = auth.decode(request.headers.authorization);
// 	const userId = request.params._id;

// 	if(userData.isAdmin){
// 		Users.findById(userId)
// 		.then(result => response.send(result))
// 		.catch(error => response.send(error));
// 	}
// 	else{
// 		return response.send('You do not have permission to access user details!');
// 	};
// };

// Setting user as admin
module.exports.updateUserType = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const userId = request.params._id;
	let updatedUser = {
		isAdmin: request.body.isAdmin
	};

	if(userData.isAdmin){
		Users.findByIdAndUpdate(userId, updatedUser)
		.then(result => response.send('You have successfully set user as admin!'))
		.catch(error => response.send(error));
	}
	else{
		return response.send('You do not have permission to set user as admin!')
	};
};

module.exports.retrieveUserDetails = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	Users.findOne({_id: userData.id})
	.then(data => response.send(data));
}