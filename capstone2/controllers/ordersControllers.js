const Orders = require('../models/ordersModel.js');
const Users = require('../models/usersModel.js');
const Carts = require('../models/cartsModel.js');
const Products = require('../models/productsModel.js');
const auth = require('../auth.js');

// Creating order
module.exports.createOrder = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const productId = request.params._id;
	const quantity = request.body.quantity;

	Products.findById(productId)
	.then(result => {
		const totalAmount = result.price * quantity;

		let newOrder = new Orders({
			userId: userData.id,
			products: [{
				productId: productId,
				quantity: quantity
			}],
			totalAmount: totalAmount,
		});

		newOrder.save()
		.then(save => response.send(true))
		.catch(error => response.send(false));
	})
	.catch(error => response.send(false));
};


// Retrieving authenticated user's orders
module.exports.getOrders = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	Orders.find({userId: userData.id})
	.then(result => {
		if(result.length === 0){
			return response.send('You do not have any order!');
		}
		else{
			return response.send(result);
		};
	})
	.catch(error => response.send(error));	
};

// Retrieving all orders
module.exports.getAllOrders = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		Orders.find({})
		.then(result => {
			if(result.length !== 0){
				return response.send(result);
			}
			else{
				return response.send('You have no orders yet!');
			};
		})
		.catch(error => response.send(error));
	}
	else{
		return response.send('You do not have permission to access all orders!');
	};
};