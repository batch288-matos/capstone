const express = require('express');
const ordersControllers = require('../controllers/ordersControllers.js');
const auth = require('../auth.js');


const router = express.Router();
// Creating order
router.post('/create/:_id', auth.verify, ordersControllers.createOrder);

// Retrieving authenticated user's order
router.get('/userOrders', auth.verify, ordersControllers.getOrders);

// Retrieving all orders
router.get('/', auth.verify, ordersControllers.getAllOrders);

// Adding product to cart
// router.post('/cart', auth.verify, ordersControllers.addProduct);


module.exports = router;