const express = require('express');
const productsControllers = require('../controllers/productsControllers.js');
const auth = require('../auth.js');

const router = express.Router();
// Creating product
router.post('/createProduct', auth.verify, productsControllers.createProduct);
// Retrieving all products
router.get('/getAllProducts', auth.verify, productsControllers.getAllProducts);
// Retrieving active products
router.get('/activeProducts', productsControllers.getActiveProducts);
// Retrieving specific product
router.get('/:_id', productsControllers.getProduct);
// Updating specific product
router.patch('/update/:_id', auth.verify, productsControllers.updateProduct);
// Archiving specific product
router.patch('/archive/:_id', auth.verify, productsControllers.archiveProduct);
// Activating specific product
router.patch('/activate/:_id', auth.verify, productsControllers.activateProduct);


module.exports =  router;