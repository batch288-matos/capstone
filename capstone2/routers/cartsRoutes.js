const express = require('express');
const cartsControllers = require('../controllers/cartsControllers.js');
const auth = require('../auth.js');

const router = express.Router();

// Add products to cart
router.post('/add/:_id', auth.verify, cartsControllers.addProducts);



module.exports = router;