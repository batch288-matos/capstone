const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const usersRoutes = require('./routers/usersRouters.js');
const productsRoutes = require('./routers/productsRoutes.js');
const ordersRoutes = require('./routers/ordersRoutes.js');
const cartsRoutes = require('./routers/cartsRoutes.js');

const port = 4002;
const app = express();

mongoose.connect('mongodb+srv://admin:admin@batch288matos.rnvh5zx.mongodb.net/E-commerceAPI?retryWrites=true&w=majority', {useNewUrlParser: true, useUnifiedTopology: true});
const database = mongoose.connection;
database.on('error', console.error.bind(console, "Error, can't connect to the database!"));
database.once('open', () => console.log('You have successfully connected to the database!'));

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

app.use('/users', usersRoutes);
app.use('/products', productsRoutes);
app.use('/orders', ordersRoutes);
app.use('/cart', cartsRoutes);

app.listen(port, () => console.log(`You are now connected to port ${port}!`));