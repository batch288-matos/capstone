import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';


// createRoot() - assigns the element to be managed by Reach with its Virtual DOM
const root = ReactDOM.createRoot(document.getElementById('root'));

// render() - displaying the component/react element in to the root
root.render(
  // <AppNavBar />
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// const name = 'John';
// const element = <h1>Hello, {name}!</h1>

// root.render(element);