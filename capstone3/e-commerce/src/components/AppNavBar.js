import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

import {Link, NavLink} from 'react-router-dom';
import {useContext} from 'react';

import UserContext from '../UserContext.js';

export default function AppNavBar(){

  const {user} = useContext(UserContext);

	return(
    <>
      <Navbar bg="dark" data-bs-theme="dark">
        <Container>
          <Navbar.Brand as = {Link} to = '/'>KJMC</Navbar.Brand>
          <Nav className="ms-auto">
            {
              user.isAdmin === true
              ?
              <>
              <Nav.Link as = {NavLink} to = '/'>Home</Nav.Link>
              <Nav.Link as = {NavLink} to = '/adminDashboard'>Admin Dashboard</Nav.Link>
              <Nav.Link as = {NavLink} to = '/logout'>Logout</Nav.Link>
              </>
              :
              localStorage.getItem('token') === null 
              ?
              <>
                <Nav.Link as = {NavLink} to = '/'>Home</Nav.Link>
                <Nav.Link as = {NavLink} to = '/products'>Products</Nav.Link>
                <Nav.Link as = {NavLink} to = '/register'>Register</Nav.Link>
                <Nav.Link as = {NavLink} to = '/login'>Login</Nav.Link>
              </>
              :
              <>
                <Nav.Link as = {NavLink} to = '/'>Home</Nav.Link>
                <Nav.Link as = {NavLink} to = '/products'>Products</Nav.Link>
                <Nav.Link as = {NavLink} to = '/logout'>Logout</Nav.Link>
              </>
            }
          </Nav>
        </Container>
      </Navbar>
    </>
	);
}