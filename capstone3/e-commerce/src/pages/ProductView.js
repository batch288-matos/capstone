import {useState, useEffect} from 'react';
import {Container, Row, Col, Button, Card} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';

import Swal2 from "sweetalert2";

export default function ProductView() {

    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');
    const [quantity, setQuantity] = useState(1);

    const {productId} = useParams();

    const navigate = useNavigate();

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
        .then(result => result.json())
        .then(data => {
            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
        })
    }, [])

    const incrementQuantity = () => {
        setQuantity(quantity + 1);
    };

    const decrementQuantity = () => {
        if (quantity > 1) {
          setQuantity(quantity - 1);
        }
    };

    const purchase = (productId) => {
        fetch(`${process.env.REACT_APP_API_URL}/orders/create/${productId}`, {
            method: "POST",
            headers: {
                "Authorization" : `Bearer ${localStorage.getItem('token')}`,
                "Content-Type" : "application/json"
            },
            body: JSON.stringify({
                quantity: quantity
            })
        })
        .then(response => response.json())
        .then(data => {
            if(data){
                Swal2.fire({
					title: 'Successfully purchased',
					icon: 'success',
					text: "You have successfully purchased this product!"
				})
				navigate('/products');
            }
            else{
                Swal2.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again.'
				})
            }
        })

    }

    return (
        <Container className='mt-3'>
            <Row>
                <Col>
                    <Card>
                        <Card.Body>
                            <Card.Title className='text-center mb-3'>{name}</Card.Title>
                                <Card.Subtitle>Description:</Card.Subtitle>
                                <Card.Text>{description}</Card.Text>
                                <Card.Subtitle>Price:</Card.Subtitle>
                                <Card.Text>{price}</Card.Text>
                                <Card.Subtitle className='mb-3'>Quantity:</Card.Subtitle>
                                <div style={{ display: 'flex', alignItems: 'center' }} className='mb-5'>
                                    <Button onClick={decrementQuantity}>-</Button>
                                    <Card.Text style={{ margin: '0 10px' }}>{quantity}</Card.Text>
                                    <Button onClick={incrementQuantity}>+</Button>
                                </div>
                                <Button variant="primary" as = {Link} to = '/products'>Back</Button>
                                <Button variant="primary" onClick={() => purchase(productId)} className='ms-1'>Buy</Button>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}