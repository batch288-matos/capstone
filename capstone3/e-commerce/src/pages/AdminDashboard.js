import {Container, Row, Col, Table, Button} from 'react-bootstrap';

import {Link} from 'react-router-dom';

import ProductTable from '../components/ProductTable.js';

import {useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext.js';

import Swal2 from 'sweetalert2';

export default function AdminDashboard(){
    const [products, setProducts] = useState([]);

    const {user} = useContext(UserContext);

	useEffect(() => {
        if(user.isAdmin === true){
            fetch(`${process.env.REACT_APP_API_URL}/products/getAllProducts`, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                    'Content-Type': 'application/json'
                }
            })
            .then(result => result.json())
            .then(data => {
                setProducts(data.map(product => {
                    return (
                        <ProductTable 
                        key = {product._id} 
                        productProp = {product} 
                        onActivate = {() => {
                            archive(product._id, true)                        
                        }}
                        onDisable = {() => {
                            archive(product._id, false)                        
                        }}
                        />
                    );
                }))
            })
        }
	})

    const archive = (productId, isActive) => {
        if(user.isAdmin === true){
            fetch(`${process.env.REACT_APP_API_URL}/products/${isActive ? "activate" : "archive"}/${productId}`, {
                method: "PATCH",
                headers: {
                    "Authorization" : `Bearer ${localStorage.getItem('token')}`,
                    "Content-Type" : "application/json"
                }
            })
            .then(response => response.json())
            .then(data => {
                if(data === true){
                    if(isActive){
                        Swal2.fire({
                            title: 'Successfully activate product!',
                            icon: 'success',
                            text: "You have successfully activate this product!"
                        })
                    }
                    else{
                        Swal2.fire({
                            title: 'Successfully archived product!',
                            icon: 'success',
                            text: "You have successfully archived this product!"
                        })
                    }
                }
                else{
                    Swal2.fire({
                        title: 'Something went wrong',
                        icon: 'error',
                        text: 'Please try again.'
                    })
                }
            })
        }
    }

    return(
        <Container className='mt-3'>
            <Row>
                <Col>
                    <h1 className='text-center'>Admin Dashboard</h1>

                    <Button as = {Link} to = '/createProduct' variant="success">Add New Product</Button>

                    <Table striped bordered hover variant="dark" className='mt-3'>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Price</th>
                                <th>Availability</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        {products}
                    </Table>
                </Col>
            </Row>
        </Container>
    )
}