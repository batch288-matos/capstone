import {Container, Row, Col, Button, Form} from 'react-bootstrap';

import {useState, useEffect} from 'react';

import {Link, useNavigate} from 'react-router-dom';

import Swal2 from 'sweetalert2';

export default function Register(){

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [confirmPassword, setConfirmPassword] = useState('');
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNumber, setMobileNumber] = useState('');

	const navigate = useNavigate();

	const [isDisabled, setIsDisabled] = useState(true);

	useEffect(() => {
		if(email !== '' && password !== '' && confirmPassword !== '' && password === confirmPassword && password.length >= 12 && firstName !== '' && lastName !== '' && mobileNumber.length >= 11){
			setIsDisabled(false);
		}
		else {
			setIsDisabled(true);
		}

	}, [email, password, confirmPassword, firstName, lastName, mobileNumber]);

	function register(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/signup`, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNumber: mobileNumber,
				password: password
			})
		})
		.then(result => result.json())
		.then(data => {
			if(data === false){
				Swal2.fire({
					title: 'Duplicate email found',
					icon: 'error',
					text: 'Please provide a different email.'
				})
			}
			else{
				Swal2.fire({
					title: 'Registration successful',
					icon: 'success',
					text: 'Please login'
				})
				navigate("/login");
			}
		})
	}

	return(
		<Container className = 'mt-3'>
			<Row>
				<Col className = 'col-6 mx-auto'>
					<h1 className = 'text-center'>Register</h1>
					<Form onSubmit = {event => register(event)}>
					<Form.Group className="mb-3" controlId="formBasicFirstName">
				        <Form.Label>First Name</Form.Label>
				        <Form.Control type="text" value = {firstName} onChange = {event => {
				        	setFirstName(event.target.value)
				        }} placeholder="Enter first name" />
				      </Form.Group>

					  <Form.Group className="mb-3" controlId="formBasicLastName">
				        <Form.Label>Last Name</Form.Label>
				        <Form.Control type="text" value = {lastName} onChange = {event => {
				        	setLastName(event.target.value)
				        }} placeholder="Enter last name" />
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicEmail">
				        <Form.Label>Email address</Form.Label>
				        <Form.Control type="email" value = {email} onChange = {event => {
				        	setEmail(event.target.value)
				        }} placeholder="Enter email" />
				      </Form.Group>

					  <Form.Group className="mb-3" controlId="formBasicMobileNumber">
				        <Form.Label>Mobile Number</Form.Label>
				        <Form.Control type="tel" value = {mobileNumber} onChange = {event => {
				        	setMobileNumber(event.target.value)
				        }} placeholder="Enter mobile number" />
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicPassword">
				        <Form.Label>Password</Form.Label>
				        <Form.Control type="password" value = {password} onChange = {event => {
				        	setPassword(event.target.value)
				        }} placeholder="Password" />
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicConfirmPassword">
				        <Form.Label>Confirm password</Form.Label>
				        <Form.Control type="password" value = {confirmPassword} onChange = {event => {
				        	setConfirmPassword(event.target.value)
				        }}placeholder="Confirm password" />
				      </Form.Group>
					  <p>Already have an account? <Link to = '/login' className = 'text-decoration-none'>Login here!</Link></p>
				      <Button variant="primary" type="submit" disabled = {isDisabled}>
				        Submit
				      </Button>
				    </Form>
				</Col>
			</Row>
		</Container>
		);
}