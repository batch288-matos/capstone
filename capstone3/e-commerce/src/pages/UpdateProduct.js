import {Container, Row, Col, Form, Button} from 'react-bootstrap';

import {Link, useNavigate, useParams} from 'react-router-dom';

import {useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext.js';

import Swal2 from 'sweetalert2';

export default function UpdateProduct(){
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');

    const {user} = useContext(UserContext);
    const {productId} = useParams();

    const navigate = useNavigate();

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
        .then(result => result.json())
        .then(data => {
            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
        })
    }, [])

    function update(event){
        if(user.isAdmin === true){
            event.preventDefault()

            fetch(`${process.env.REACT_APP_API_URL}/products/update/${productId}`, {
                method: "PATCH",
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`,
                    'Content-Type' : 'application/json'
                },
                body: JSON.stringify({
                    name: name,
                    description: description,
                    price: price
                })
            })
            .then(result => result.json())
            .then(data => {
                if(data === true){
                    Swal2.fire({
                        title: 'Updated product successfully!',
                        icon: 'success',
                        text: 'You may update another product!'
                    })
                }
                navigate('/adminDashboard');
            })
        }
	}
    
    return(
        <Container className='mt-3'>
            <Row>
                <Col>
                    <h1 className='text-center'>Update Product</h1>
                    <Form className='mx-auto col-6' onSubmit = {event => update(event)}>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label>Name:</Form.Label>
                            <Form.Control type="text" value = {name} onChange = {event => {
                                setName(event.target.value)}}/>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                            <Form.Label>Description:</Form.Label>
                            <Form.Control as="textarea" rows={3} value = {description} onChange = {event => {
                                setDescription(event.target.value)}}/>
                        </Form.Group>

                        <Form.Group className="mb-3 col-2" controlId="exampleForm.ControlInput2">
                            <Form.Label>Price:</Form.Label>
                            <Form.Control type="number" value = {price} onChange = {event => {
                                setPrice(event.target.value)}}/>
                        </Form.Group>
                        
                        <Button as = {Link} to = '/adminDashboard' variant="danger" className='me-1'>Cancel</Button>
                        <Button variant="success" type="submit" className='me-1'>Update</Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}