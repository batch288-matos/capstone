import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import AppNavBar from './components/AppNavBar.js';
import Home from './pages/Home.js';
import Products from './pages/Products.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import PageNotFound from './pages/PageNotFound.js';
import ProductView from './pages/ProductView.js';
import AdminDashboard from './pages/AdminDashboard.js';
import CreateProduct from './pages/CreateProduct.js';
import UpdateProduct from './pages/UpdateProduct.js';

import {BrowserRouter, Route, Routes} from 'react-router-dom';
import {useState, useEffect} from 'react';
import {UserProvider} from './UserContext.js';

function App() {
  const [user, setUser] = useState('');
  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {
    if(localStorage.getItem('token')) {
      fetch(`${process.env.REACT_APP_API_URL}/users/retrieveUserDetails`, {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(result => result.json())
      .then(data => {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      })
    }
  }, [])


  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
      <BrowserRouter>
        <Routes>
          <Route path = '/' element = {
            <>
            <AppNavBar/>
            <Home/>
            </>
          }/>
          {
            user.isAdmin === true
            ?
            <>
              <Route path = '/adminDashboard' element = {
                <>
                <AppNavBar/>
                <AdminDashboard/>
                </>
              }/>
              <Route path = '/createProduct' element = {
                <>
                <AppNavBar/>
                <CreateProduct/>
                </>
              }/>
              <Route path = '/adminDashboard/products/:productId' element = {
                <>
                <AppNavBar/>
                <UpdateProduct/>  
                </>
              }/>
              <Route path = '/products' element = {<PageNotFound/>}/>
              <Route path = '/products/:productId' element = {<PageNotFound/>}/>
            </>
            :
            <>
              <Route path = '/products' element = {
                <>
                <AppNavBar/>
                <Products/>
                </>
              }/>
              <Route path = '/products/:productId' element = {
                <>
                <AppNavBar/>
                <ProductView/>
                </>
              }/>
              <Route path = '/adminDashboard' element = {<PageNotFound/>}/>
              <Route path = '/createProduct' element = {<PageNotFound/>}/>
              <Route path = '/adminDashboard/products/:productId' element = {<PageNotFound/>}/>
            </>
          }
          {
            localStorage.getItem('token') === null
            ?
            <>
            <Route path = '/register' element = {
              <>
              <AppNavBar/>
              <Register/>
              </>
            }/>
            <Route path = '/login' element = {
              <>
              <AppNavBar/>
              <Login/>
              </>
            }/>
            </>
            :
            <>
            <Route path = '/register' element = {<PageNotFound/>}/>
            <Route path = '/login' element = {<PageNotFound/>}/>
            </>
          }
          <Route path = '/logout' element = {
            <>
            <AppNavBar/>
            <Logout/>
            </>
          }/>
          <Route path ='*' element = {<PageNotFound/>}/>
        </Routes>
      </BrowserRouter>
    </UserProvider>
  );
}

export default App;
